#pragma once

#include "cartitem.h"

const int MAX_CART_ITEMS = 10;

class Cart {
private:
    CartItem* cartItems[MAX_CART_ITEMS];
    int cartItemCount;
public:
    Cart();
    ~Cart();
    Cart& operator +=(const Item& item);
    Money getTotal() const;
    void clear();
    friend std::ostream &operator << (std::ostream &, const Cart &);
};
