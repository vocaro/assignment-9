#include "item.h"
#include "money.h"

using namespace std;

unsigned int Item::nextSKU = 1000;

Item::Item(string description, float _price) : price(_price)
{
    this->description = description;
    sku = nextSKU++;
}

Item::operator string()
{
    return description + " " + (string)price;
}

bool Item::operator ==(const Item& right)
{
    return sku == right.sku;
}

Money Item::getPrice() const
{
    return price;
}