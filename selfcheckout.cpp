#include "selfcheckout.h"

#include <iostream>

using namespace std;

void SelfCheckoutTerminal::start()
{
    cout << "Self Checkout Terminal" << endl << endl;

    while (true) {
        
        cout << "Command? ";
        
        char command;
        cin >> command;
        
        switch (command) {
            case 'a':
            int itemIndex;
            cin >> itemIndex;
            cart += inventory[itemIndex - 1];
            break;
            
            case 'c':
            cout << cart;
            break;
            
            case 'i':
            cout << inventory;
            break;
            
            case 'p':
            cout << "You were charged " << cart.getTotal() << endl;
            cart.clear();
            break;

            case 'x':
            exit(0);
            break;
            
            default:
            cout << "Invalid command." << endl;
        }
    }
}
