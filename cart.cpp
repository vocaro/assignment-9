#include "cart.h"

using namespace std;

Cart::Cart()
{
    cartItemCount = 0;
}

Cart::~Cart()
{
    clear();
}

Cart& Cart::operator +=(const Item& item)
{
    if (cartItemCount == MAX_CART_ITEMS) {
        return *this;
    }
    
    for (int i = 0; i < cartItemCount; i++) {
        if (cartItems[i]->item == item) {
            cartItems[i]->quantity++;
            return *this;
        }
    }

    CartItem *cartItem = new CartItem(item);
    cartItems[cartItemCount++] = cartItem;
    
    return *this;
}

Money Cart::getTotal() const
{
    Money total(0);

    for (int i = 0; i < cartItemCount; i++) {
        total += cartItems[i]->getTotal();
    }

    return total;
}

void Cart::clear()
{
    for (int i = 0; i < cartItemCount; i++) {
        delete cartItems[i];
    }    
    
    cartItemCount = 0;    
}

ostream& operator << (ostream &strm, const Cart &cart)
{
    for (int i = 0; i < cart.cartItemCount; i++) {
        CartItem cartItem = *cart.cartItems[i];
        strm << (string)cartItem << endl;
    }
    strm << "Total: " << (string)cart.getTotal() << endl;
    return strm;
}
