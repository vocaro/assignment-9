#pragma once

#include "money.h"

#include <iostream>
#include <string>

class Item {
private:
    std::string description;
    Money price;
    unsigned int sku;
    static unsigned int nextSKU;
public:
    Item(std::string description, float price);
    operator std::string();
    bool operator ==(const Item& right);
    Money getPrice() const;
};
