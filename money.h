#pragma once

#include <string>

class Money {
private:
    int cents;
public:
    Money(float amount);
    operator std::string();
    Money operator * (int x);
    Money& operator +=(const Money& money);
};
