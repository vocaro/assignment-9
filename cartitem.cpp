#include "cartitem.h"

#include <sstream>

using namespace std;

CartItem::CartItem(Item _item) : item(_item)
{
    quantity = 1;
}

CartItem::operator string()
{
    ostringstream ss;
    ss << (string)item
       << " x "
       << quantity
       << " = "
       << (string)getTotal();
    return ss.str();
}

Money CartItem::getTotal() const
{
    return item.getPrice() * quantity;
}
