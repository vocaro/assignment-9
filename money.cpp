#include "money.h"

#include <iomanip>
#include <sstream>

using namespace std;

Money::Money(float amount)
{
    float integral = (int)amount;
    float fractional = amount - integral;
    cents = integral * 100 + fractional * 100;
}

Money::operator string()
{
    ostringstream ss;
    ss << "$"
       << (cents / 100)
       << "."
       << setw(2) << setfill('0')
       << (cents % 100);
    return ss.str();
}

Money Money::operator * (int x)
{
    cents *= x;
    return *this;
}

Money& Money::operator += (const Money& money)
{
    cents += money.cents;
    return *this;
}
