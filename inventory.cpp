#include "inventory.h"

#include <iostream>

using namespace std;

Inventory::Inventory()
{
    items[0] = new Item("Apple", 1.00);
    items[1] = new Item("Banana", 2.00);
    items[2] = new Item("Cherry", 3.00);
}

Inventory::~Inventory()
{
    for (int i = 0; i < MAX_INVENTORY_ITEMS; i++) {
        delete items[i];
    }
}

Item& Inventory::operator[](const int& index)
{
    if (index < 0 || index >= MAX_INVENTORY_ITEMS) {
        abort();
    }
    
    return *items[index];
}

ostream &operator << (ostream &strm, const Inventory &inventory)
{
    for (int i = 0; i < MAX_INVENTORY_ITEMS; i++) {
        strm << (i+1) << ": " << (string)*(inventory.items[i]) << endl;
    }
    return strm;
}
