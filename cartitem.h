#pragma once

#include "item.h"

#include <iostream>
#include <string>

class Cart;

class CartItem
{
private:
    Item item;
    int quantity;    
    friend class Cart;
    friend std::ostream& operator << (std::ostream &strm, const Cart &cart);
public:
    CartItem(Item item);
    operator std::string();
    Money getTotal() const;
};
