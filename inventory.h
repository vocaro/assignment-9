#pragma once

#include "item.h"

#include <iostream>

const int MAX_INVENTORY_ITEMS = 3;

class Inventory {
private:
    Item *items[MAX_INVENTORY_ITEMS];
public:
    Inventory();
    ~Inventory();
    Item& operator[](const int&);
    friend std::ostream &operator << (std::ostream &, const Inventory &);
};
